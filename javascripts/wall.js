var Wall = function(scene, pos, scale, dir) {
	var matMap = THREE.ImageUtils.loadTexture( "images/fire.jpg" );
	var material = new THREE.MeshBasicMaterial({
		map : matMap,
		transparent : true
	});
	var geometry = new THREE.PlaneGeometry( 1, 1 );
	this.mesh = new THREE.Mesh( geometry, material );
	this.mesh.scale = scale;
	scene.add( this.mesh );
	
	this.mesh.position.x = pos.x;
	this.mesh.position.y = pos.y;
	this.mesh.z = 0;

	this.speed = 0.1;
	this.randomCounter = 40;
	
	this.otherWall = null;
	this.minDistance = 0;
	this.maxDistance = 0;
	this.setOtherWall = function(wall, minDistance, maxDistance) {
		this.otherWall = wall;
		wall.otherWall = this;
		this.minDistance = wall.minDistance = minDistance;
		this.maxDistance = wall.maxDistance = maxDistance;
	};
	
	this.update = function( delta ) {
		this.mesh.position.x += dir.x * this.speed;
		this.mesh.position.y += dir.y * this.speed;
		this.randomCounter--;
		if (this.randomCounter < 0) {
			this.reverse();
		}

		var xDistance = Math.abs(this.mesh.position.x - this.otherWall.mesh.position.x);
		var yDistance = Math.abs(this.mesh.position.y - this.otherWall.mesh.position.y);
		console.log(xDistance + "," + yDistance + "\t" + this.minDistance + "," + this.maxDistance);
		if (Math.max(xDistance, yDistance) < this.minDistance) {
			// Reverse if it's moving towards the other wall
			if (Math.abs(this.mesh.position.x + dir.x - this.otherWall.mesh.position.x) < xDistance ||
					Math.abs(this.mesh.position.y + dir.y - this.otherWall.mesh.position.y) < yDistance) {
				this.reverse();
			}
		} else if (Math.max(xDistance, yDistance) > this.maxDistance) {
			// Reverse if it's moving away from the other wall
			if (Math.abs(this.mesh.position.x + dir.x - this.otherWall.mesh.position.x) > xDistance ||
					Math.abs(this.mesh.position.y + dir.y - this.otherWall.mesh.position.y) > yDistance) {
				this.reverse();
			}
		}
	};
	this.reverse = function() {
		this.randomCounter = Math.random() * 60 + 10;
		dir.x *= -1;
		dir.y *= -1;
	};
	
	this.detectCollision = function( pos, scale) {
		return pos.x - scale.x / 4 < this.mesh.position.x + this.mesh.scale.x /2 && 
			pos.x + scale.x / 4 > this.mesh.position.x - this.mesh.scale.x /2 &&
			pos.y - scale.y / 4 < this.mesh.position.y + this.mesh.scale.y /2 &&
			pos.y + scale.y / 4 > this.mesh.position.y - this.mesh.scale.y /2		
	};
};